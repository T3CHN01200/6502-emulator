#include <stdio.h>
#include <stdint.h>

#ifndef __MEMORY_H_

typedef enum : uint8_t
{
	BLOCK_TYPE_RAM,
	BLOCK_TYPE_ROM,
	BLOCK_TYPE_FILE,
	BLOCK_TYPE_STDIO,
} BlockType;

// The addresses are inclusive
typedef struct
{
	union
	{
		uint8_t *memory;
		FILE *file;
	};
	uint64_t blockStartAddress;
	uint64_t blockEndAddress;
	BlockType blockType;
} Addressable;

void ReadBlock8(uint8_t *data, Addressable *block, uint64_t addr);
void WriteBlock8(uint8_t data, Addressable *block, uint64_t addr);
void ReadBlock16(uint16_t *data, Addressable *block, uint64_t addr);
void WriteBlock16(uint16_t data, Addressable *block, uint64_t addr);
void ReadBlock32(uint32_t *data, Addressable *block, uint64_t addr);
void WriteBlock32(uint32_t data, Addressable *block, uint64_t addr);
void ReadBlock64(uint64_t *data, Addressable *block, uint64_t addr);
void WriteBlock64(uint64_t data, Addressable *block, uint64_t addr);
void *GetPtrToAddrBlock(Addressable *block, uint64_t addr);

#define __MEMORY_H_
#endif
