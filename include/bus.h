#include <stddef.h>
#include <stdint.h>

#include "memory.h"

#ifndef __BUS_H_

typedef struct
{
	void *object;
	void(*Tick)(void *);
} Tickable;

struct Bus
{
	size_t tickableCount;
	Tickable *tickables;
	size_t blockCount;
	Addressable* blocks;
	int64_t clockNanos;
	int64_t nextClockNanos;
	union
	{
		uint8_t status;
		struct
		{
			uint8_t waitForClock : 1;
		};
	};
};

typedef struct Bus Bus;

void Tick_Bus(Bus *bus);

void Read8(uint8_t *data, Bus *bus, uint64_t addr);
void Write8(uint8_t data, Bus *bus, uint64_t addr);
void Read16(uint16_t *data, Bus *bus, uint64_t addr);
void Write16(uint16_t data, Bus *bus, uint64_t addr);
void Read32(uint32_t *data, Bus *bus, uint64_t addr);
void Write32(uint32_t data, Bus *bus, uint64_t addr);
void Read64(uint64_t *data, Bus *bus, uint64_t addr);
void Write64(uint64_t data, Bus *bus, uint64_t addr);
void *GetPtrToAddr(Bus *bus, uint64_t addr);

#define __BUS_H_
#endif
