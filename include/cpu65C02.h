#ifndef __CPU65C02_H_

#include "bus.h"
#include "cpu6502.h"

#include <stdint.h>

typedef enum : uint8_t
{
	ADDRM_65C02_ABS,
	ADDRM_65C02_IDX_IND_X,
	ADDRM_65C02_IDX_X,
	ADDRM_65C02_IDX_Y,
	ADDRM_65C02_ABS_IND,
	ADDRM_65C02_ACC,
	ADDRM_65C02_IMM,
	ADDRM_65C02_IMP,
	ADDRM_65C02_PCR,
	ADDRM_65C02_SP,
	ADDRM_65C02_ZP,
	ADDRM_65C02_ZP_IND_X,
	ADDRM_65C02_ZP_IDX_X,
	ADDRM_65C02_ZP_IDX_Y,
	ADDRM_65C02_ZP_IND,
	ADDRM_65C02_ZP_IND_Y,
} AddressingMode65C02;

typedef union
{
    CPU6502Instruction cpu6502Instruction;
    struct {
    	union
    	{
    		int8_t opi8;
    		uint16_t opu16;
    	};
    	uint8_t byte;
    	uint8_t ticks;
    	AddressingMode65C02 addrMode;
    };
} CPU65C02Instruction;

typedef struct
{
    Bus *bus;
    union
    {
    	uint16_t pc;
    	struct
    	{
    		uint8_t pcl;
    		uint8_t pch;
    	};
    };
    uint8_t a;
    uint8_t x;
    uint8_t y;
    uint8_t sp;
    uint8_t instructionClocks;
    union
    {
    	uint8_t status;
    	struct
    	{
    		uint8_t carry : 1;
    		uint8_t zero : 1;
    		uint8_t irqDisable : 1;
    		uint8_t decimal : 1;
    		uint8_t brk : 1;
    		uint8_t unused : 1;
    		uint8_t overflow : 1;
    		uint8_t negative : 1;
    	};
    };
    uint8_t irqSet : 1;
    uint8_t nmiSet : 1;
} CPU65C02;

void New_CPU65C02Instruction_IndexedIndirect(
	CPU65C02Instruction *dst,
	uint8_t instruction,
	CPU65C02 *cpu
);
void New_CPU65C02Instruction_ZeroPageIndirect(
	CPU65C02Instruction *dst,
	uint8_t instruction,
	CPU65C02 *cpu
);

Def6502(65C02)

void CPU65C02_BBR(CPU65C02 *cpu, CPU65C02Instruction *instruction);
void CPU65C02_BBS(CPU65C02 *cpu, CPU65C02Instruction *instruction);
void CPU65C02_BRA(CPU65C02 *cpu, CPU65C02Instruction *instruction);
void CPU65C02_PHX(CPU65C02 *cpu, CPU65C02Instruction *instruction);
void CPU65C02_PHY(CPU65C02 *cpu, CPU65C02Instruction *instruction);
void CPU65C02_PLX(CPU65C02 *cpu, CPU65C02Instruction *instruction);
void CPU65C02_PLY(CPU65C02 *cpu, CPU65C02Instruction *instruction);
void CPU65C02_RMB(CPU65C02 *cpu, CPU65C02Instruction *instruction);
void CPU65C02_SMB(CPU65C02 *cpu, CPU65C02Instruction *instruction);
void CPU65C02_STP(CPU65C02 *cpu, CPU65C02Instruction *instruction);
void CPU65C02_STZ(CPU65C02 *cpu, CPU65C02Instruction *instruction);
void CPU65C02_TRB(CPU65C02 *cpu, CPU65C02Instruction *instruction);
void CPU65C02_TSB(CPU65C02 *cpu, CPU65C02Instruction *instruction);
void CPU65C02_WAI(CPU65C02 *cpu, CPU65C02Instruction *instruction);

#define __CPU65C02_H_
#endif
