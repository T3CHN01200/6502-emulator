#ifndef __CPU6502_H_

#include "bus.h"

#include <stdint.h>

typedef enum : uint8_t
{
	ADDRM_6502_ABS,
	ADDRM_6502_IDX_X,
	ADDRM_6502_IDX_Y,
	ADDRM_6502_ABS_IND,
	ADDRM_6502_ACC,
	ADDRM_6502_IMM,
	ADDRM_6502_IMP,
	ADDRM_6502_PCR,
	ADDRM_6502_SP,
	ADDRM_6502_ZP,
	ADDRM_6502_ZP_IND_X,
	ADDRM_6502_ZP_IDX_X,
	ADDRM_6502_ZP_IDX_Y,
	ADDRM_6502_ZP_IND_Y,
} AddressingMode6502;

typedef struct
{
	union
	{
		int8_t opi8;
		uint16_t opu16;
	};
	uint8_t byte;
	uint8_t ticks;
	AddressingMode6502 addrMode;
} CPU6502Instruction;

typedef struct
{
	Bus *bus;
	union
	{
		uint16_t pc;
		struct
		{
			uint8_t pcl;
			uint8_t pch;
		};
	};
	uint8_t a;
	uint8_t x;
	uint8_t y;
	uint8_t sp;
	uint8_t instructionClocks;
	union
	{
		uint8_t status;
		struct
		{
			uint8_t carry : 1;
			uint8_t zero : 1;
			uint8_t irqDisable : 1;
			uint8_t decimal : 1;
			uint8_t brk : 1;
			uint8_t unused : 1;
			uint8_t overflow : 1;
			uint8_t negative : 1;
		};
	};
	uint8_t irqSet : 1;
	uint8_t nmiSet : 1;
} CPU6502;

#define Def6502(variant) \
void Tick_CPU##variant##_void(void *cpu); \
void TickUnbound_CPU##variant##_void(void *cpu); \
void Tick_CPU##variant(CPU##variant *cpu); \
void TickUnbound_CPU##variant(CPU##variant *cpu); \
 \
void New_CPU##variant##Instruction_Absolute( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
); \
void New_CPU##variant##Instruction_IndexedX( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
); \
void New_CPU##variant##Instruction_IndexedY( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
); \
void New_CPU##variant##Instruction_AbsoluteIndirect( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
); \
void New_CPU##variant##Instruction_Accumulator( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
); \
void New_CPU##variant##Instruction_Immediate( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
); \
void New_CPU##variant##Instruction_Implied( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
); \
void New_CPU##variant##Instruction_ProgramCounterRelative( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
); \
void New_CPU##variant##Instruction_Stack( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
); \
void New_CPU##variant##Instruction_ZeroPage( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
); \
void New_CPU##variant##Instruction_ZeroPageIndexedIndirectX( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
); \
void New_CPU##variant##Instruction_ZeroPageIndirectIndexedY( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
); \
void New_CPU##variant##Instruction_ZeroPageIndexedX( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
); \
void New_CPU##variant##Instruction_ZeroPageIndexedY( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
); \
void CPU##variant##_ADC(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_AND(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_ASL(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_BCC(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_BCS(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_BEQ(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_BIT(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_BMI(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_BNE(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_BPL(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_BRK(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_BVC(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_BVS(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_CLC(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_CLD(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_CLI(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_CLV(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_CMP(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_CPX(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_CPY(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_DEC(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_DEX(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_DEY(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_EOR(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_INC(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_INX(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_INY(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_JMP(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_JSR(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_LDA(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_LDX(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_LDY(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_LSR(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_NOP(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_ORA(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_PHA(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_PHP(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_PLA(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_PLP(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_ROL(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_ROR(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_RTI(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_RTS(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_SBC(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_SEC(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_SED(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_SEI(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_STA(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_STX(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_STY(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_TAX(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_TAY(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_TSX(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_TXA(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_TXS(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
void CPU##variant##_TYA(CPU##variant *cpu, CPU##variant##Instruction *instruction); \
 \
void CPU##variant##_ResetDefault(CPU##variant *cpu); \
void CPU##variant##_Reset(CPU##variant *cpu, uint8_t status); \
void CPU##variant##_IRQ(CPU##variant *cpu); \
void CPU##variant##_NMI(CPU##variant *cpu);

Def6502(6502)

#define __CPU6502_H_
#endif
