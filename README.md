# Introduction

This is a 6502 emulator (or more accurately, a w65C02s emulator). I know there
are many like it and many of which are better, but this one is mine.

It was spawned out of wanting to follow along with
[Ben Eater's 6502 computer series](https://www.youtube.com/playlist?list=PLowKtXNTBypFbtuVMUVXNR0z1mu7dp7eH)
and potentially getting to the point of writing emulators for other 6502 based
computers. This is also a way to get a (relatively) quick to implement emulator
out to test against for another project regarding a front end for a variety of
cpu emulators to be plugged into. Again, namely, one for an 8086 following
[Computer Enhance](https://www.computerenhance.com/).
