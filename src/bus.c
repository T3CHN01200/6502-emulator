#include "bus.h"
#include "memory.h"

#include <stdint.h>
#define __USE_POSIX199309
#include <time.h>

typedef struct timespec timespec;

int64_t GetCurrentTimeNsec()
{
	timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	return (ts.tv_sec * 1e9) + ts.tv_nsec;
}

void Tick_Bus(Bus *bus)
{
	const Tickable *endTickablePtr = bus->tickables + bus->tickableCount;
	for (; bus->waitForClock & (GetCurrentTimeNsec() < bus->nextClockNanos);)
	{
	}
	bus->nextClockNanos = GetCurrentTimeNsec() + bus->clockNanos;
	for (Tickable *ptr = bus->tickables; ptr < endTickablePtr; ++ptr)
	{
		ptr->Tick(ptr->object);
	}
}

#define ImplRW(width) \
void Read##width(uint##width##_t *data, Bus *bus, uint64_t addr) \
{ \
	const Addressable *endBlockPtr = bus->blocks + bus->blockCount; \
	for (Addressable *ptr = bus->blocks; ptr < endBlockPtr; ++ptr) \
	{ \
		if ((addr >= ptr->blockStartAddress) & (addr <= ptr->blockEndAddress)) \
		{ \
			ReadBlock##width(data, ptr, addr); \
		} \
	} \
} \
 \
void Write##width(uint##width##_t data, Bus *bus, uint64_t addr) \
{ \
	const Addressable *endBlockPtr = bus->blocks + bus->blockCount; \
	for (Addressable *ptr = bus->blocks; ptr < endBlockPtr; ++ptr) \
	{ \
		if ((addr >= ptr->blockStartAddress) & (addr <= ptr->blockEndAddress)) \
		{ \
			WriteBlock##width(data, ptr, addr); \
		} \
	} \
}

ImplRW(8)
ImplRW(16)
ImplRW(32)
ImplRW(64)

void *GetPtrToAddr(Bus *bus, uint64_t addr)
{
	const Addressable *endBlockPtr = bus->blocks + bus->blockCount;
	void *ret = 0;
	for (Addressable *ptr = bus->blocks; ptr < endBlockPtr; ++ptr)
	{
		if ((addr >= ptr->blockStartAddress) & (addr <= ptr->blockEndAddress))
		{
			ret = GetPtrToAddrBlock(ptr, addr);
			break;
		}
	}
	return ret;
}
