#include "memory.h"

#define ImplRW(width) \
void ReadBlock##width(uint##width##_t *data, Addressable *block, uint64_t addr) \
{ \
	switch (block->blockType) \
	{ \
		case (BLOCK_TYPE_RAM): \
		case (BLOCK_TYPE_ROM): \
		{ \
			*data = *(block->memory + (addr - block->blockStartAddress)); \
		} break; \
		case (BLOCK_TYPE_FILE): \
		{ \
			fread(data, (width) >> 3, 1, block->file); \
		} break; \
		case (BLOCK_TYPE_STDIO): \
		{ \
			fread(data, (width) >> 3, 1, stdin); \
		} break; \
	} \
} \
 \
void WriteBlock##width(uint##width##_t data, Addressable *block, uint64_t addr) \
{ \
	switch (block->blockType) \
	{ \
		case (BLOCK_TYPE_RAM): \
		{ \
			*(block->memory + (addr - block->blockStartAddress)) = data; \
		} break; \
		case (BLOCK_TYPE_FILE): \
		{ \
			fwrite(&data, (width) >> 3, 1, block->file); \
			fflush(block->file); \
		} break; \
		case (BLOCK_TYPE_STDIO): \
		{ \
			fwrite(&data, (width) >> 3, 1, stdout); \
			fflush(stdout); \
		} break; \
		default: \
		{ \
		} break; \
	} \
}

ImplRW(8)
ImplRW(16)
ImplRW(32)
ImplRW(64)

void *GetPtrToAddrBlock(Addressable *block, uint64_t addr)
{
	switch (block->blockType)
	{
		case (BLOCK_TYPE_RAM):
		case (BLOCK_TYPE_ROM):
		{
			return block->memory + (addr - block->blockStartAddress);
		} break;
		default:
		{
			return 0;
		} break;
	}
}
