#include <assert.h>
#include <stdint.h>

#include "bus.h"
#include "cpu65C02.h"

void(*AddressingModeMatrix65C02[])(CPU65C02Instruction *, uint8_t, CPU65C02 *) =
{
	/*0*/
	New_CPU65C02Instruction_Implied,
	New_CPU65C02Instruction_ZeroPageIndexedIndirectX,
	0,
	0,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_Stack,
	New_CPU65C02Instruction_Immediate,
	New_CPU65C02Instruction_Accumulator,
	0,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_ProgramCounterRelative,
	/*1*/
	New_CPU65C02Instruction_ProgramCounterRelative,
	New_CPU65C02Instruction_ZeroPageIndirectIndexedY,
	New_CPU65C02Instruction_ZeroPageIndirect,
	0,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_ZeroPageIndexedX,
	New_CPU65C02Instruction_ZeroPageIndexedX,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_Implied,
	New_CPU65C02Instruction_IndexedY,
	New_CPU65C02Instruction_Accumulator,
	0,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_IndexedX,
	New_CPU65C02Instruction_IndexedX,
	New_CPU65C02Instruction_ProgramCounterRelative,
	/*2*/
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_ZeroPageIndexedX,
	0,
	0,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_Stack,
	New_CPU65C02Instruction_Immediate,
	New_CPU65C02Instruction_Accumulator,
	0,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_ProgramCounterRelative,
	/*3*/
	New_CPU65C02Instruction_ProgramCounterRelative,
	New_CPU65C02Instruction_ZeroPageIndirectIndexedY,
	New_CPU65C02Instruction_ZeroPageIndirect,
	0,
	New_CPU65C02Instruction_ZeroPageIndexedX,
	New_CPU65C02Instruction_ZeroPageIndexedX,
	New_CPU65C02Instruction_ZeroPageIndexedX,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_Implied,
	New_CPU65C02Instruction_IndexedY,
	New_CPU65C02Instruction_Accumulator,
	0,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_ProgramCounterRelative,
	/*4*/
	New_CPU65C02Instruction_Stack,
	New_CPU65C02Instruction_ZeroPageIndexedIndirectX,
	0,
	0,
	0,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_Stack,
	New_CPU65C02Instruction_Immediate,
	New_CPU65C02Instruction_Accumulator,
	0,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_ProgramCounterRelative,
	/*5*/
	New_CPU65C02Instruction_ProgramCounterRelative,
	New_CPU65C02Instruction_ZeroPageIndirectIndexedY,
	New_CPU65C02Instruction_ZeroPageIndirect,
	0,
	0,
	New_CPU65C02Instruction_ZeroPageIndexedX,
	New_CPU65C02Instruction_ZeroPageIndexedX,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_Implied,
	New_CPU65C02Instruction_IndexedY,
	New_CPU65C02Instruction_Stack,
	0,
	0,
	New_CPU65C02Instruction_IndexedX,
	New_CPU65C02Instruction_IndexedX,
	New_CPU65C02Instruction_ProgramCounterRelative,
	/*6*/
	New_CPU65C02Instruction_Stack,
	New_CPU65C02Instruction_ZeroPageIndexedIndirectX,
	0,
	0,
	New_CPU65C02Instruction_ZeroPageIndirect,
	New_CPU65C02Instruction_ZeroPageIndirect,
	New_CPU65C02Instruction_ZeroPageIndirect,
	New_CPU65C02Instruction_ZeroPageIndirect,
	New_CPU65C02Instruction_Stack,
	New_CPU65C02Instruction_Immediate,
	New_CPU65C02Instruction_Accumulator,
	0,
	New_CPU65C02Instruction_AbsoluteIndirect,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_ProgramCounterRelative,
	/*7*/
	New_CPU65C02Instruction_ProgramCounterRelative,
	New_CPU65C02Instruction_ZeroPageIndirectIndexedY,
	New_CPU65C02Instruction_ZeroPageIndirect,
	0,
	New_CPU65C02Instruction_ZeroPageIndexedX,
	New_CPU65C02Instruction_ZeroPageIndexedX,
	New_CPU65C02Instruction_ZeroPageIndexedX,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_Implied,
	New_CPU65C02Instruction_IndexedY,
	New_CPU65C02Instruction_Stack,
	0,
	New_CPU65C02Instruction_IndexedIndirect,
	New_CPU65C02Instruction_IndexedX,
	New_CPU65C02Instruction_IndexedX,
	New_CPU65C02Instruction_ProgramCounterRelative,
	/*8*/
	New_CPU65C02Instruction_ProgramCounterRelative,
	New_CPU65C02Instruction_ZeroPageIndexedIndirectX,
	0,
	0,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_Implied,
	New_CPU65C02Instruction_Immediate,
	New_CPU65C02Instruction_Implied,
	0,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_ProgramCounterRelative,
	/*9*/
	New_CPU65C02Instruction_ProgramCounterRelative,
	New_CPU65C02Instruction_ZeroPageIndirectIndexedY,
	New_CPU65C02Instruction_ZeroPageIndirect,
	0,
	New_CPU65C02Instruction_ZeroPageIndexedX,
	New_CPU65C02Instruction_ZeroPageIndexedX,
	New_CPU65C02Instruction_ZeroPageIndexedY,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_Implied,
	New_CPU65C02Instruction_IndexedY,
	New_CPU65C02Instruction_Implied,
	0,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_IndexedX,
	New_CPU65C02Instruction_IndexedX,
	New_CPU65C02Instruction_ProgramCounterRelative,
	/*A*/
	New_CPU65C02Instruction_Immediate,
	New_CPU65C02Instruction_ZeroPageIndexedIndirectX,
	New_CPU65C02Instruction_Immediate,
	0,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_Implied,
	New_CPU65C02Instruction_Immediate,
	New_CPU65C02Instruction_Implied,
	0,
	New_CPU65C02Instruction_Accumulator,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_ProgramCounterRelative,
	/*B*/
	New_CPU65C02Instruction_ProgramCounterRelative,
	New_CPU65C02Instruction_ZeroPageIndirectIndexedY,
	New_CPU65C02Instruction_ZeroPageIndirect,
	0,
	New_CPU65C02Instruction_ZeroPageIndexedIndirectX,
	New_CPU65C02Instruction_ZeroPageIndexedIndirectX,
	New_CPU65C02Instruction_ZeroPageIndirectIndexedY,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_Implied,
	New_CPU65C02Instruction_IndexedY,
	New_CPU65C02Instruction_Implied,
	0,
	New_CPU65C02Instruction_IndexedX,
	New_CPU65C02Instruction_IndexedX,
	New_CPU65C02Instruction_IndexedY,
	New_CPU65C02Instruction_ProgramCounterRelative,
	/*C*/
	New_CPU65C02Instruction_Immediate,
	New_CPU65C02Instruction_ZeroPageIndexedY,
	0,
	0,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_Implied,
	New_CPU65C02Instruction_Immediate,
	New_CPU65C02Instruction_Implied,
	New_CPU65C02Instruction_Implied,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_ProgramCounterRelative,
	/*D*/
	New_CPU65C02Instruction_ProgramCounterRelative,
	New_CPU65C02Instruction_ZeroPageIndirectIndexedY,
	New_CPU65C02Instruction_ZeroPageIndirect,
	0,
	0,
	New_CPU65C02Instruction_ZeroPageIndexedX,
	New_CPU65C02Instruction_ZeroPageIndexedX,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_Implied,
	New_CPU65C02Instruction_IndexedY,
	New_CPU65C02Instruction_Stack,
	New_CPU65C02Instruction_Implied,
	0,
	New_CPU65C02Instruction_IndexedX,
	New_CPU65C02Instruction_IndexedX,
	New_CPU65C02Instruction_ProgramCounterRelative,
	/*E*/
	New_CPU65C02Instruction_Immediate,
	New_CPU65C02Instruction_ZeroPageIndexedIndirectX,
	0,
	0,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_Implied,
	New_CPU65C02Instruction_Immediate,
	New_CPU65C02Instruction_Implied,
	0,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_Absolute,
	New_CPU65C02Instruction_ProgramCounterRelative,
	/*F*/
	New_CPU65C02Instruction_ProgramCounterRelative,
	New_CPU65C02Instruction_ZeroPageIndirectIndexedY,
	New_CPU65C02Instruction_ZeroPageIndexedY,
	0,
	0,
	New_CPU65C02Instruction_ZeroPageIndexedX,
	New_CPU65C02Instruction_ZeroPageIndexedX,
	New_CPU65C02Instruction_ZeroPage,
	New_CPU65C02Instruction_Implied,
	New_CPU65C02Instruction_IndexedY,
	New_CPU65C02Instruction_Stack,
	0,
	0,
	New_CPU65C02Instruction_IndexedX,
	New_CPU65C02Instruction_IndexedX,
	New_CPU65C02Instruction_ProgramCounterRelative,
};

void(*OpCodeMatrix65C02[])(CPU65C02 *, CPU65C02Instruction *) =
{
	/*0*/
	CPU65C02_BRK,
	CPU65C02_ORA,
	0,
	0,
	CPU65C02_TSB,
	CPU65C02_ORA,
	CPU65C02_ASL,
	CPU65C02_RMB,
	CPU65C02_PHP,
	CPU65C02_ORA,
	CPU65C02_ASL,
	0,
	CPU65C02_TSB,
	CPU65C02_ORA,
	CPU65C02_ASL,
	CPU65C02_BBR,
	/*1*/
	CPU65C02_BPL,
	CPU65C02_ORA,
	CPU65C02_ORA,
	0,
	CPU65C02_TRB,
	CPU65C02_ORA,
	CPU65C02_ASL,
	CPU65C02_RMB,
	CPU65C02_CLC,
	CPU65C02_ORA,
	CPU65C02_INC,
	0,
	CPU65C02_TRB,
	CPU65C02_ORA,
	CPU65C02_ASL,
	CPU65C02_BBR,
	/*2*/
	CPU65C02_JSR,
	CPU65C02_AND,
	0,
	0,
	CPU65C02_BIT,
	CPU65C02_AND,
	CPU65C02_ROL,
	CPU65C02_RMB,
	CPU65C02_PLP,
	CPU65C02_AND,
	CPU65C02_ROL,
	0,
	CPU65C02_BIT,
	CPU65C02_AND,
	CPU65C02_ROL,
	CPU65C02_BBR,
	/*3*/
	CPU65C02_BMI,
	CPU65C02_AND,
	CPU65C02_AND,
	0,
	CPU65C02_BIT,
	CPU65C02_AND,
	CPU65C02_ROL,
	CPU65C02_RMB,
	CPU65C02_SEC,
	CPU65C02_AND,
	CPU65C02_DEC,
	0,
	CPU65C02_BIT,
	CPU65C02_AND,
	CPU65C02_ROL,
	CPU65C02_BBR,
	/*4*/
	CPU65C02_RTI,
	CPU65C02_EOR,
	0,
	0,
	0,
	CPU65C02_EOR,
	CPU65C02_LSR,
	CPU65C02_RMB,
	CPU65C02_PHA,
	CPU65C02_EOR,
	CPU65C02_LSR,
	0,
	CPU65C02_JMP,
	CPU65C02_EOR,
	CPU65C02_LSR,
	CPU65C02_BBR,
	/*5*/
	CPU65C02_BVC,
	CPU65C02_EOR,
	CPU65C02_EOR,
	0,
	0,
	CPU65C02_EOR,
	CPU65C02_LSR,
	CPU65C02_RMB,
	CPU65C02_CLI,
	CPU65C02_EOR,
	CPU65C02_PHY,
	0,
	0,
	CPU65C02_EOR,
	CPU65C02_LSR,
	CPU65C02_BBR,
	/*6*/
	CPU65C02_RTS,
	CPU65C02_ADC,
	0,
	0,
	CPU65C02_STZ,
	CPU65C02_ADC,
	CPU65C02_ROR,
	CPU65C02_RMB,
	CPU65C02_PLA,
	CPU65C02_ADC,
	CPU65C02_ROR,
	0,
	CPU65C02_JMP,
	CPU65C02_ADC,
	CPU65C02_ROR,
	CPU65C02_BBR,
	/*7*/
	CPU65C02_BVS,
	CPU65C02_ADC,
	CPU65C02_ADC,
	0,
	CPU65C02_STZ,
	CPU65C02_ADC,
	CPU65C02_ROR,
	CPU65C02_RMB,
	CPU65C02_SEI,
	CPU65C02_ADC,
	CPU65C02_PLY,
	0,
	CPU65C02_JMP,
	CPU65C02_ADC,
	CPU65C02_ROR,
	CPU65C02_BBR,
	/*8*/
	CPU65C02_BRA,
	CPU65C02_STA,
	0,
	0,
	CPU65C02_STY,
	CPU65C02_STA,
	CPU65C02_STX,
	CPU65C02_SMB,
	CPU65C02_DEY,
	CPU65C02_BIT,
	CPU65C02_TXA,
	0,
	CPU65C02_STY,
	CPU65C02_STA,
	CPU65C02_STX,
	CPU65C02_BBS,
	/*9*/
	CPU65C02_BCC,
	CPU65C02_STA,
	CPU65C02_STA,
	0,
	CPU65C02_STY,
	CPU65C02_STA,
	CPU65C02_STX,
	CPU65C02_SMB,
	CPU65C02_TYA,
	CPU65C02_STA,
	CPU65C02_TSX,
	0,
	CPU65C02_STZ,
	CPU65C02_STA,
	CPU65C02_STZ,
	CPU65C02_BBS,
	/*A*/
	CPU65C02_LDY,
	CPU65C02_LDA,
	CPU65C02_LDX,
	0,
	CPU65C02_LDY,
	CPU65C02_LDA,
	CPU65C02_LDX,
	CPU65C02_SMB,
	CPU65C02_TAY,
	CPU65C02_LDA,
	CPU65C02_TAX,
	0,
	CPU65C02_LDY,
	CPU65C02_LDA,
	CPU65C02_LDX,
	CPU65C02_BBS,
	/*B*/
	CPU65C02_BCS,
	CPU65C02_LDA,
	CPU65C02_LDA,
	0,
	CPU65C02_LDY,
	CPU65C02_LDA,
	CPU65C02_LDX,
	CPU65C02_SMB,
	CPU65C02_CLV,
	CPU65C02_LDA,
	CPU65C02_TSX,
	0,
	CPU65C02_LDY,
	CPU65C02_LDA,
	CPU65C02_LDX,
	CPU65C02_BBS,
	/*C*/
	CPU65C02_CPY,
	CPU65C02_CMP,
	0,
	0,
	CPU65C02_CPY,
	CPU65C02_CMP,
	CPU65C02_DEC,
	CPU65C02_SMB,
	CPU65C02_INY,
	CPU65C02_CMP,
	CPU65C02_DEX,
	CPU65C02_WAI,
	CPU65C02_CPY,
	CPU65C02_CMP,
	CPU65C02_DEC,
	CPU65C02_BBS,
	/*D*/
	CPU65C02_BNE,
	CPU65C02_CMP,
	CPU65C02_CMP,
	0,
	0,
	CPU65C02_CMP,
	CPU65C02_DEC,
	CPU65C02_SMB,
	CPU65C02_CLD,
	CPU65C02_CMP,
	CPU65C02_PHX,
	CPU65C02_STP,
	0,
	CPU65C02_CMP,
	CPU65C02_DEC,
	CPU65C02_BBS,
	/*E*/
	CPU65C02_CPX,
	CPU65C02_SBC,
	0,
	0,
	CPU65C02_CPX,
	CPU65C02_SBC,
	CPU65C02_INC,
	CPU65C02_SMB,
	CPU65C02_INX,
	CPU65C02_SBC,
	CPU65C02_NOP,
	0,
	CPU65C02_CPX,
	CPU65C02_SBC,
	CPU65C02_INC,
	CPU65C02_BBS,
	/*F*/
	CPU65C02_BEQ,
	CPU65C02_SBC,
	CPU65C02_SBC,
	0,
	0,
	CPU65C02_SBC,
	CPU65C02_INC,
	CPU65C02_SMB,
	CPU65C02_SED,
	CPU65C02_SBC,
	CPU65C02_PLX,
	0,
	0,
	CPU65C02_SBC,
	CPU65C02_INC,
	CPU65C02_BBS,
};

void(*AddressingModeMatrix6502[])(CPU6502Instruction *, uint8_t, CPU6502 *) =
{
	/*0*/
	New_CPU6502Instruction_Implied,
	New_CPU6502Instruction_ZeroPageIndexedIndirectX,
	0,
	0,
	0,
	New_CPU6502Instruction_ZeroPage,
	New_CPU6502Instruction_ZeroPage,
	0,
	New_CPU6502Instruction_Stack,
	New_CPU6502Instruction_Immediate,
	New_CPU6502Instruction_Accumulator,
	0,
	0,
	New_CPU6502Instruction_Absolute,
	New_CPU6502Instruction_Absolute,
	0,
	/*1*/
	New_CPU6502Instruction_ProgramCounterRelative,
	New_CPU6502Instruction_ZeroPageIndirectIndexedY,
	0,
	0,
	0,
	New_CPU6502Instruction_ZeroPageIndexedX,
	New_CPU6502Instruction_ZeroPageIndexedX,
	0,
	New_CPU6502Instruction_Implied,
	New_CPU6502Instruction_IndexedY,
	0,
	0,
	0,
	New_CPU6502Instruction_IndexedX,
	New_CPU6502Instruction_IndexedX,
	0,
	/*2*/
	New_CPU6502Instruction_Absolute,
	New_CPU6502Instruction_ZeroPageIndexedX,
	0,
	0,
	New_CPU6502Instruction_ZeroPage,
	New_CPU6502Instruction_ZeroPage,
	New_CPU6502Instruction_ZeroPage,
	0,
	New_CPU6502Instruction_Stack,
	New_CPU6502Instruction_Immediate,
	New_CPU6502Instruction_Accumulator,
	0,
	New_CPU6502Instruction_Absolute,
	New_CPU6502Instruction_Absolute,
	New_CPU6502Instruction_Absolute,
	0,
	/*3*/
	New_CPU6502Instruction_ProgramCounterRelative,
	New_CPU6502Instruction_ZeroPageIndirectIndexedY,
	0,
	0,
	New_CPU6502Instruction_ZeroPageIndexedX,
	New_CPU6502Instruction_ZeroPageIndexedX,
	New_CPU6502Instruction_ZeroPageIndexedX,
	0,
	New_CPU6502Instruction_Implied,
	New_CPU6502Instruction_IndexedY,
	0,
	0,
	0,
	New_CPU6502Instruction_Absolute,
	New_CPU6502Instruction_Absolute,
	0,
	/*4*/
	New_CPU6502Instruction_Stack,
	New_CPU6502Instruction_ZeroPageIndexedIndirectX,
	0,
	0,
	0,
	New_CPU6502Instruction_ZeroPage,
	New_CPU6502Instruction_ZeroPage,
	0,
	New_CPU6502Instruction_Stack,
	New_CPU6502Instruction_Immediate,
	New_CPU6502Instruction_Accumulator,
	0,
	New_CPU6502Instruction_Absolute,
	New_CPU6502Instruction_Absolute,
	New_CPU6502Instruction_Absolute,
	0,
	/*5*/
	New_CPU6502Instruction_ProgramCounterRelative,
	New_CPU6502Instruction_ZeroPageIndirectIndexedY,
	0,
	0,
	0,
	New_CPU6502Instruction_ZeroPageIndexedX,
	New_CPU6502Instruction_ZeroPageIndexedX,
	0,
	New_CPU6502Instruction_Implied,
	New_CPU6502Instruction_IndexedY,
	0,
	0,
	0,
	New_CPU6502Instruction_IndexedX,
	New_CPU6502Instruction_IndexedX,
	0,
	/*6*/
	New_CPU6502Instruction_Stack,
	New_CPU6502Instruction_ZeroPageIndexedIndirectX,
	0,
	0,
	0,
	New_CPU6502Instruction_ZeroPage,
	New_CPU6502Instruction_ZeroPage,
	0,
	New_CPU6502Instruction_Stack,
	New_CPU6502Instruction_Immediate,
	New_CPU6502Instruction_Accumulator,
	0,
	New_CPU6502Instruction_AbsoluteIndirect,
	New_CPU6502Instruction_Absolute,
	New_CPU6502Instruction_Absolute,
	0,
	/*7*/
	New_CPU6502Instruction_ProgramCounterRelative,
	New_CPU6502Instruction_ZeroPageIndirectIndexedY,
	0,
	0,
	0,
	New_CPU6502Instruction_ZeroPageIndexedX,
	New_CPU6502Instruction_ZeroPageIndexedX,
	0,
	New_CPU6502Instruction_Implied,
	New_CPU6502Instruction_IndexedY,
	0,
	0,
	0,
	New_CPU6502Instruction_IndexedX,
	New_CPU6502Instruction_IndexedX,
	0,
	/*8*/
	0,
	New_CPU6502Instruction_ZeroPageIndexedIndirectX,
	0,
	0,
	0,
	New_CPU6502Instruction_ZeroPage,
	New_CPU6502Instruction_ZeroPage,
	0,
	New_CPU6502Instruction_Implied,
	0,
	New_CPU6502Instruction_Implied,
	0,
	New_CPU6502Instruction_Absolute,
	New_CPU6502Instruction_Absolute,
	New_CPU6502Instruction_Absolute,
	0,
	/*9*/
	New_CPU6502Instruction_ProgramCounterRelative,
	New_CPU6502Instruction_ZeroPageIndirectIndexedY,
	0,
	0,
	New_CPU6502Instruction_ZeroPageIndexedX,
	New_CPU6502Instruction_ZeroPageIndexedX,
	New_CPU6502Instruction_ZeroPageIndexedY,
	0,
	New_CPU6502Instruction_Implied,
	New_CPU6502Instruction_IndexedY,
	New_CPU6502Instruction_Implied,
	0,
	0,
	New_CPU6502Instruction_IndexedX,
	0,
	0,
	/*A*/
	New_CPU6502Instruction_Immediate,
	New_CPU6502Instruction_ZeroPageIndexedIndirectX,
	New_CPU6502Instruction_Immediate,
	0,
	New_CPU6502Instruction_ZeroPage,
	New_CPU6502Instruction_ZeroPage,
	New_CPU6502Instruction_ZeroPage,
	0,
	New_CPU6502Instruction_Implied,
	New_CPU6502Instruction_Immediate,
	New_CPU6502Instruction_Implied,
	0,
	New_CPU6502Instruction_Accumulator,
	New_CPU6502Instruction_Absolute,
	New_CPU6502Instruction_Absolute,
	0,
	/*B*/
	New_CPU6502Instruction_ProgramCounterRelative,
	New_CPU6502Instruction_ZeroPageIndirectIndexedY,
	0,
	0,
	New_CPU6502Instruction_ZeroPageIndexedIndirectX,
	New_CPU6502Instruction_ZeroPageIndexedIndirectX,
	New_CPU6502Instruction_ZeroPageIndirectIndexedY,
	0,
	New_CPU6502Instruction_Implied,
	New_CPU6502Instruction_IndexedY,
	New_CPU6502Instruction_Implied,
	0,
	New_CPU6502Instruction_IndexedX,
	New_CPU6502Instruction_IndexedX,
	New_CPU6502Instruction_IndexedY,
	0,
	/*C*/
	New_CPU6502Instruction_Immediate,
	New_CPU6502Instruction_ZeroPageIndexedY,
	0,
	0,
	New_CPU6502Instruction_ZeroPage,
	New_CPU6502Instruction_ZeroPage,
	New_CPU6502Instruction_ZeroPage,
	0,
	New_CPU6502Instruction_Implied,
	New_CPU6502Instruction_Immediate,
	New_CPU6502Instruction_Implied,
	0,
	New_CPU6502Instruction_Absolute,
	New_CPU6502Instruction_Absolute,
	New_CPU6502Instruction_Absolute,
	0,
	/*D*/
	New_CPU6502Instruction_ProgramCounterRelative,
	New_CPU6502Instruction_ZeroPageIndirectIndexedY,
	0,
	0,
	0,
	New_CPU6502Instruction_ZeroPageIndexedX,
	New_CPU6502Instruction_ZeroPageIndexedX,
	0,
	New_CPU6502Instruction_Implied,
	New_CPU6502Instruction_IndexedY,
	0,
	0,
	0,
	New_CPU6502Instruction_IndexedX,
	New_CPU6502Instruction_IndexedX,
	0,
	/*E*/
	New_CPU6502Instruction_Immediate,
	New_CPU6502Instruction_ZeroPageIndexedIndirectX,
	0,
	0,
	New_CPU6502Instruction_ZeroPage,
	New_CPU6502Instruction_ZeroPage,
	New_CPU6502Instruction_ZeroPage,
	0,
	New_CPU6502Instruction_Implied,
	New_CPU6502Instruction_Immediate,
	New_CPU6502Instruction_Implied,
	0,
	New_CPU6502Instruction_Absolute,
	New_CPU6502Instruction_Absolute,
	New_CPU6502Instruction_Absolute,
	0,
	/*F*/
	New_CPU6502Instruction_ProgramCounterRelative,
	New_CPU6502Instruction_ZeroPageIndirectIndexedY,
	0,
	0,
	0,
	New_CPU6502Instruction_ZeroPageIndexedX,
	New_CPU6502Instruction_ZeroPageIndexedX,
	0,
	New_CPU6502Instruction_Implied,
	New_CPU6502Instruction_IndexedY,
	0,
	0,
	0,
	New_CPU6502Instruction_IndexedX,
	New_CPU6502Instruction_IndexedX,
	0,
};

void(*OpCodeMatrix6502[])(CPU6502 *, CPU6502Instruction *) =
{
	/*0*/
	CPU6502_BRK,
	CPU6502_ORA,
	0,
	0,
	0,
	CPU6502_ORA,
	CPU6502_ASL,
	0,
	CPU6502_PHP,
	CPU6502_ORA,
	CPU6502_ASL,
	0,
	0,
	CPU6502_ORA,
	CPU6502_ASL,
	0,
	/*1*/
	CPU6502_BPL,
	CPU6502_ORA,
	0,
	0,
	0,
	CPU6502_ORA,
	CPU6502_ASL,
	0,
	CPU6502_CLC,
	CPU6502_ORA,
	0,
	0,
	0,
	CPU6502_ORA,
	CPU6502_ASL,
	0,
	/*2*/
	CPU6502_JSR,
	CPU6502_AND,
	0,
	0,
	CPU6502_BIT,
	CPU6502_AND,
	CPU6502_ROL,
	0,
	CPU6502_PLP,
	CPU6502_AND,
	CPU6502_ROL,
	0,
	CPU6502_BIT,
	CPU6502_AND,
	CPU6502_ROL,
	0,
	/*3*/
	CPU6502_BMI,
	CPU6502_AND,
	0,
	0,
	CPU6502_BIT,
	CPU6502_AND,
	CPU6502_ROL,
	0,
	CPU6502_SEC,
	CPU6502_AND,
	0,
	0,
	0,
	CPU6502_AND,
	CPU6502_ROL,
	0,
	/*4*/
	CPU6502_RTI,
	CPU6502_EOR,
	0,
	0,
	0,
	CPU6502_EOR,
	CPU6502_LSR,
	0,
	CPU6502_PHA,
	CPU6502_EOR,
	CPU6502_LSR,
	0,
	CPU6502_JMP,
	CPU6502_EOR,
	CPU6502_LSR,
	0,
	/*5*/
	CPU6502_BVC,
	CPU6502_EOR,
	0,
	0,
	0,
	CPU6502_EOR,
	CPU6502_LSR,
	0,
	CPU6502_CLI,
	CPU6502_EOR,
	0,
	0,
	0,
	CPU6502_EOR,
	CPU6502_LSR,
	0,
	/*6*/
	CPU6502_RTS,
	CPU6502_ADC,
	0,
	0,
	0,
	CPU6502_ADC,
	CPU6502_ROR,
	0,
	CPU6502_PLA,
	CPU6502_ADC,
	CPU6502_ROR,
	0,
	CPU6502_JMP,
	CPU6502_ADC,
	CPU6502_ROR,
	0,
	/*7*/
	CPU6502_BVS,
	CPU6502_ADC,
	0,
	0,
	0,
	CPU6502_ADC,
	CPU6502_ROR,
	0,
	CPU6502_SEI,
	CPU6502_ADC,
	0,
	0,
	0,
	CPU6502_ADC,
	CPU6502_ROR,
	0,
	/*8*/
	0,
	CPU6502_STA,
	0,
	0,
	0,
	CPU6502_STA,
	CPU6502_STX,
	0,
	CPU6502_DEY,
	0,
	CPU6502_TXA,
	0,
	CPU6502_STY,
	CPU6502_STA,
	CPU6502_STX,
	0,
	/*9*/
	CPU6502_BCC,
	CPU6502_STA,
	0,
	0,
	CPU6502_STY,
	CPU6502_STA,
	CPU6502_STX,
	0,
	CPU6502_TYA,
	CPU6502_STA,
	CPU6502_TSX,
	0,
	0,
	CPU6502_STA,
	0,
	0,
	/*A*/
	CPU6502_LDY,
	CPU6502_LDA,
	CPU6502_LDX,
	0,
	CPU6502_LDY,
	CPU6502_LDA,
	CPU6502_LDX,
	0,
	CPU6502_TAY,
	CPU6502_LDA,
	CPU6502_TAX,
	0,
	CPU6502_LDY,
	CPU6502_LDA,
	CPU6502_LDX,
	0,
	/*B*/
	CPU6502_BCS,
	CPU6502_LDA,
	0,
	0,
	CPU6502_LDY,
	CPU6502_LDA,
	CPU6502_LDX,
	0,
	CPU6502_CLV,
	CPU6502_LDA,
	CPU6502_TSX,
	0,
	CPU6502_LDY,
	CPU6502_LDA,
	CPU6502_LDX,
	0,
	/*C*/
	CPU6502_CPY,
	CPU6502_CMP,
	0,
	0,
	CPU6502_CPY,
	CPU6502_CMP,
	CPU6502_DEC,
	0,
	CPU6502_INY,
	CPU6502_CMP,
	CPU6502_DEX,
	0,
	CPU6502_CPY,
	CPU6502_CMP,
	CPU6502_DEC,
	0,
	/*D*/
	CPU6502_BNE,
	CPU6502_CMP,
	0,
	0,
	0,
	CPU6502_CMP,
	CPU6502_DEC,
	0,
	CPU6502_CLD,
	CPU6502_CMP,
	0,
	0,
	0,
	CPU6502_CMP,
	CPU6502_DEC,
	0,
	/*E*/
	CPU6502_CPX,
	CPU6502_SBC,
	0,
	0,
	CPU6502_CPX,
	CPU6502_SBC,
	CPU6502_INC,
	0,
	CPU6502_INX,
	CPU6502_SBC,
	CPU6502_NOP,
	0,
	CPU6502_CPX,
	CPU6502_SBC,
	CPU6502_INC,
	0,
	/*F*/
	CPU6502_BEQ,
	CPU6502_SBC,
	0,
	0,
	0,
	CPU6502_SBC,
	CPU6502_INC,
	0,
	CPU6502_SED,
	CPU6502_SBC,
	0,
	0,
	0,
	CPU6502_SBC,
	CPU6502_INC,
	0,
};

#define Impl6502(variant) \
void Tick_CPU##variant##_void(void *cpu) \
{ \
	Tick_CPU##variant(cpu); \
} \
 \
void TickUnbound_##variant##_void(void *cpu) \
{ \
	TickUnbound_CPU##variant(cpu); \
} \
 \
void Tick_CPU##variant(CPU##variant *cpu) \
{ \
	if ((cpu->instructionClocks)--) \
	{ \
		return; \
	} \
	TickUnbound_CPU##variant(cpu); \
} \
 \
void TickUnbound_CPU##variant(CPU##variant *cpu) \
{ \
	uint8_t byte = 0; \
	CPU##variant##Instruction instruction; \
	Read8(&byte, cpu->bus, cpu->pc); \
	AddressingModeMatrix##variant[byte](&instruction, byte, cpu); \
	OpCodeMatrix##variant[byte](cpu, &instruction); \
	cpu->instructionClocks = instruction.ticks; \
	(cpu->pc)++; \
	CPU##variant##_IRQ(cpu); \
	CPU##variant##_NMI(cpu); \
} \
 \
void New_CPU##variant##Instruction_Absolute( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
) \
{ \
	uint8_t operand[2]; \
	(cpu->pc)++; \
	Read8(operand, cpu->bus, cpu->pc); \
	(cpu->pc)++; \
	Read8(operand + 1, cpu->bus, cpu->pc); \
	dst->opu16 = *((uint16_t*)operand); \
	dst->ticks = 4; \
	dst->addrMode = ADDRM_##variant##_ABS; \
} \
 \
void New_CPU##variant##Instruction_IndexedX( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
) \
{ \
	union { \
		uint8_t u8s[2]; \
		uint16_t u16; \
	} operand; \
	(cpu->pc)++; \
	Read8(operand.u8s, cpu->bus, cpu->pc); \
	(cpu->pc)++; \
	Read8(operand.u8s + 1, cpu->bus, cpu->pc); \
	dst->opu16 = operand.u16 + cpu->x; \
	dst->ticks = 4 + (((operand.u16) & 0XFF) == 0XFF); \
	dst->addrMode = ADDRM_##variant##_IDX_X; \
} \
 \
void New_CPU##variant##Instruction_IndexedY( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
) \
{ \
	union { \
		uint8_t u8s[2]; \
		uint16_t u16; \
	} operand; \
	(cpu->pc)++; \
	Read8(operand.u8s, cpu->bus, cpu->pc); \
	(cpu->pc)++; \
	Read8(operand.u8s + 1, cpu->bus, cpu->pc); \
	dst->opu16 = operand.u16 + cpu->y; \
	dst->ticks = 4 + (((operand.u16) & 0XFF) == 0XFF); \
	dst->addrMode = ADDRM_##variant##_IDX_Y; \
} \
 \
void New_CPU##variant##Instruction_Accumulator( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
) \
{ \
	dst->ticks = 2; \
	dst->addrMode = ADDRM_##variant##_ACC; \
} \
 \
void New_CPU##variant##Instruction_Immediate( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
) \
{ \
	uint8_t operand; \
	(cpu->pc)++; \
	dst->opu16 = cpu->pc; \
	dst->ticks = 2; \
	dst->addrMode = ADDRM_##variant##_IMM; \
} \
 \
void New_CPU##variant##Instruction_Implied( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
) \
{ \
	dst->ticks = 2; \
	dst->addrMode = ADDRM_##variant##_IMP; \
} \
 \
void New_CPU##variant##Instruction_ProgramCounterRelative( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
) \
{ \
	int8_t operand; \
	(cpu->pc)++; \
	Read8((uint8_t*) &operand, cpu->bus, cpu->pc); \
	dst->opi8 = operand; \
	dst->ticks = 2 + (((cpu->pc + dst->opi8) & 0XFF) == 0XFF); \
	dst->addrMode = ADDRM_##variant##_PCR; \
} \
 \
void New_CPU##variant##Instruction_Stack( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
) \
{ \
	dst->ticks = 3; \
	dst->addrMode = ADDRM_##variant##_SP; \
} \
 \
void New_CPU##variant##Instruction_ZeroPage( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
) \
{ \
	uint8_t operand; \
	(cpu->pc)++; \
	Read8(&operand, cpu->bus, cpu->pc); \
	dst->opu16 = operand; \
	dst->ticks = 3; \
	dst->addrMode = ADDRM_##variant##_ZP; \
} \
 \
void New_CPU##variant##Instruction_ZeroPageIndexedIndirectX( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
) \
{ \
	uint8_t operand; \
	(cpu->pc)++; \
	Read8(&operand, cpu->bus, cpu->pc); \
	dst->opu16 = *(uint16_t*)GetPtrToAddr(cpu->bus, operand + cpu->x); \
	dst->ticks = 6; \
	dst->addrMode = ADDRM_##variant##_ZP_IND_X; \
} \
 \
void New_CPU##variant##Instruction_ZeroPageIndirectIndexedY( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
) \
{ \
	uint8_t operand; \
	(cpu->pc)++; \
	Read8(&operand, cpu->bus, cpu->pc); \
	operand += cpu->y; \
	dst->opu16 = *(uint16_t*)GetPtrToAddr(cpu->bus, operand); \
	dst->ticks = 5 + (operand == 0XFF); \
	dst->addrMode = ADDRM_##variant##_ZP_IND_Y; \
} \
 \
void New_CPU##variant##Instruction_ZeroPageIndexedX( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
) \
{ \
	uint8_t operand; \
	(cpu->pc)++; \
	Read8(&operand, cpu->bus, cpu->pc); \
	dst->opu16 = operand + cpu->x; \
	dst->ticks = 4; \
	dst->addrMode = ADDRM_##variant##_ZP_IDX_X; \
} \
 \
void New_CPU##variant##Instruction_ZeroPageIndexedY( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
) \
{ \
	uint8_t operand; \
	(cpu->pc)++; \
	Read8(&operand, cpu->bus, cpu->pc); \
	dst->opu16 = operand + cpu->y; \
	dst->ticks = 4; \
	dst->addrMode = ADDRM_##variant##_ZP_IDX_Y; \
} \
 \
void CPU##variant##_ADC(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	uint8_t m; \
	uint16_t sum = 0; \
	Read8(&m, cpu->bus, instruction->opu16); \
	sum = cpu->a + m + cpu->carry; \
	cpu->status = cpu->status & 0X7D; \
	if (sum > 255) \
	{ \
		cpu->status |= 0X82; \
	} \
	cpu->negative = (sum & 0X80) == 0X80; \
	cpu->a = sum & 0XFF; \
	cpu->zero = !(cpu->a); \
} \
 \
void CPU##variant##_AND(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	uint8_t m; \
	uint16_t result = 0; \
	Read8(&m, cpu->bus, instruction->opu16); \
	result = cpu->a & m; \
	cpu->status = cpu->status & 0XBF; \
	if (result > 255) \
	{ \
		cpu->status |= 0X82; \
	} \
	cpu->negative = (result & 0X80) == 0X80; \
	cpu->a = result & 0XFF; \
	cpu->zero = !(cpu->a); \
} \
 \
void CPU##variant##_ASL(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	uint8_t *m; \
	uint8_t value; \
	switch (instruction->addrMode) \
	{ \
		case ADDRM_##variant##_ACC: \
		{ \
			m = &(cpu->a); \
		} break; \
		default: \
		{ \
			m = GetPtrToAddr(cpu->bus, instruction->opu16); \
		} \
	} \
	value = *m; \
	cpu->carry = (0X80 & value) == 0X80;  \
	value = (value << 1) & 0XFE; \
	cpu->negative = (value & 0X80) == 0X80; \
	cpu->zero = value == 0; \
	*m = value; \
} \
 \
void CPU##variant##_BCC(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert(instruction->addrMode == ADDRM_##variant##_PCR); \
	cpu->pc += instruction->opi8 * !cpu->carry; \
} \
 \
void CPU##variant##_BCS(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert(instruction->addrMode == ADDRM_##variant##_PCR); \
	cpu->pc += instruction->opi8 * cpu->carry; \
} \
 \
void CPU##variant##_BEQ(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert(instruction->addrMode == ADDRM_##variant##_PCR); \
	cpu->pc += instruction->opi8 * cpu->zero; \
} \
 \
void CPU##variant##_BIT(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	uint8_t m; \
	Read8(&m, cpu->bus, instruction->opu16); \
	cpu->status = (cpu->status & 0X3F) | (m & 0XC0); \
	cpu->zero = m == 0; \
} \
 \
void CPU##variant##_BMI(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert(instruction->addrMode == ADDRM_##variant##_PCR); \
	cpu->pc += instruction->opi8 * cpu->negative; \
} \
 \
void CPU##variant##_BNE(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert(instruction->addrMode == ADDRM_##variant##_PCR); \
	cpu->pc += instruction->opi8 * !cpu->zero; \
} \
 \
void CPU##variant##_BPL(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert(instruction->addrMode == ADDRM_##variant##_PCR); \
	cpu->pc += instruction->opi8 * !cpu->negative; \
} \
 \
void CPU##variant##_BRK(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	cpu->status = (cpu->status & 0XC7) | 0X28; \
} \
 \
void CPU##variant##_BVC(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert(instruction->addrMode == ADDRM_##variant##_PCR); \
	cpu->pc += instruction->opi8 * !cpu->overflow; \
} \
 \
void CPU##variant##_BVS(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert(instruction->addrMode == ADDRM_##variant##_PCR); \
	cpu->pc += instruction->opi8 * cpu->overflow; \
} \
 \
void CPU##variant##_CLC(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	if (instruction->addrMode != ADDRM_##variant##_IMP) \
	{ \
		assert(0); \
	} \
	cpu->carry = 0; \
} \
 \
void CPU##variant##_CLD(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert(instruction->addrMode == ADDRM_##variant##_IMP); \
	cpu->decimal = 0; \
} \
 \
void CPU##variant##_CLI(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert(instruction->addrMode == ADDRM_##variant##_IMP); \
	cpu->irqDisable = 0; \
} \
 \
void CPU##variant##_CLV(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert(instruction->addrMode == ADDRM_##variant##_IMP); \
	cpu->overflow = 0; \
} \
 \
void CPU##variant##_CMP(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	uint8_t m; \
	uint16_t difference = 0; \
	Read8(&m, cpu->bus, instruction->opu16); \
	difference = cpu->a - m; \
	cpu->status = cpu->status & 0X7D; \
	if (difference > 255) \
	{ \
		cpu->status |= 0X82; \
	} \
	cpu->negative = (difference & 0X80) == 0X80; \
	cpu->zero = !(difference); \
} \
 \
void CPU##variant##_CPX(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	uint8_t m; \
	uint16_t difference = 0; \
	Read8(&m, cpu->bus, instruction->opu16); \
	difference = cpu->x - m; \
	cpu->status = cpu->status & 0X7D; \
	if (difference > 255) \
	{ \
		cpu->status |= 0X82; \
	} \
	cpu->negative = (difference & 0X80) == 0X80; \
	cpu->zero = !(difference); \
} \
 \
void CPU##variant##_CPY(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	uint8_t m; \
	uint16_t difference = 0; \
	Read8(&m, cpu->bus, instruction->opu16); \
	difference = cpu->y - m; \
	cpu->status = cpu->status & 0X7D; \
	if (difference > 255) \
	{ \
		cpu->status |= 0X82; \
	} \
	cpu->negative = (difference & 0X80) == 0X80; \
	cpu->zero = !(difference); \
} \
 \
void CPU##variant##_DEC(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	uint8_t value; \
	uint8_t *m = GetPtrToAddr(cpu->bus, instruction->opu16); \
	(*m)--; \
	value = *m; \
	cpu->status = (cpu->status & 0X7D) | \
		(value & 0X80) | \
		((value != 0X0) << 1); \
} \
 \
void CPU##variant##_DEX(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert(instruction->addrMode == ADDRM_##variant##_IMP); \
	(cpu->x)--; \
	cpu->status = (cpu->status & 0X7D) | \
		(cpu->x & 0X80) | \
		((cpu->x != 0X0) << 1); \
} \
 \
void CPU##variant##_DEY(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert(instruction->addrMode == ADDRM_##variant##_IMP); \
	(cpu->y)--; \
	cpu->status = (cpu->status & 0X7D) | \
		(cpu->y & 0X80) | \
		((cpu->y != 0X0) << 1); \
} \
 \
void CPU##variant##_EOR(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	uint8_t m; \
	Read8(&m, cpu->bus, instruction->opu16); \
	cpu->a ^= m; \
	cpu->status = cpu->status & 0XFD; \
	if (cpu->a > 255) \
	{ \
		cpu->status |= 0X82; \
	} \
	cpu->negative = (cpu->a & 0X80) == 0X80; \
	cpu->zero = !(cpu->a); \
} \
 \
void CPU##variant##_INC(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	uint8_t *m; \
	uint8_t value; \
	m = GetPtrToAddr(cpu->bus, instruction->opu16); \
	(*m)++; \
	value = *m; \
	cpu->status = (cpu->status & 0X7D) | \
		(value & 0X80) | \
		((value != 0X0) << 1); \
} \
 \
void CPU##variant##_INX(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert(instruction->addrMode == ADDRM_##variant##_IMP); \
	(cpu->x)++; \
	cpu->status = (cpu->status & 0X7D) | \
		(cpu->x & 0X80) | \
		((cpu->x != 0X0) << 1); \
} \
 \
void CPU##variant##_INY(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert(instruction->addrMode == ADDRM_##variant##_IMP); \
	(cpu->y)++; \
	cpu->status = (cpu->status & 0X7D) | \
		(cpu->y & 0X80) | \
		((cpu->y != 0X0) << 1); \
} \
 \
void CPU##variant##_JMP(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	switch (instruction->addrMode) \
	{ \
		case ADDRM_##variant##_ABS: \
		{ \
			cpu->pc = instruction->opu16; \
		} break; \
		default: \
		{ \
			Read8(&(cpu->pcl), cpu->bus, instruction->opu16); \
			Read8(&(cpu->pch), cpu->bus, instruction->opu16 + 1); \
		} break; \
	} \
	cpu->pc -= 1; \
} \
 \
void CPU##variant##_JSR(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert(instruction->addrMode == ADDRM_##variant##_ABS); \
	Write8(cpu->pch, cpu->bus, 0X100 | cpu->sp); \
	cpu->sp--; \
	Write8(cpu->pcl, cpu->bus, 0X100 | cpu->sp); \
	cpu->sp--; \
	cpu->status = (cpu->status & 0X7D) | \
		(cpu->a & 0X80) | \
		((cpu->a != 0X0) << 1); \
} \
 \
void CPU##variant##_LDA(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	uint8_t m; \
	Read8(&m, cpu->bus, instruction->opu16); \
	cpu->a = m; \
	cpu->status = (cpu->status & 0X7D) | \
		(cpu->a & 0X80) | \
		((cpu->a != 0X0) << 1); \
} \
 \
void CPU##variant##_LDX(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	uint8_t m; \
	Read8(&m, cpu->bus, instruction->opu16); \
	cpu->x = m; \
	cpu->status = (cpu->status & 0X7D) | \
		(cpu->x & 0X80) | \
		((cpu->x != 0X0) << 1); \
} \
 \
void CPU##variant##_LDY(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	uint8_t m; \
	Read8(&m, cpu->bus, instruction->opu16); \
	cpu->y = m; \
	cpu->status = (cpu->status & 0X7D) | \
		(cpu->y & 0X80) | \
		((cpu->y != 0X0) << 1); \
} \
 \
void CPU##variant##_LSR(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	uint8_t *m; \
	uint8_t value; \
	switch (instruction->addrMode) \
	{ \
		case ADDRM_##variant##_ACC: \
		{ \
			m = &(cpu->a); \
		} break; \
		default: \
		{ \
			m = GetPtrToAddr(cpu->bus, instruction->opu16); \
		} break; \
	} \
	value = *m; \
	cpu->carry = 0X01 & value;  \
	value = (value >> 1) & 0X7F; \
	cpu->negative = 0; \
	cpu->zero = value == 0; \
	*m = value; \
} \
 \
void CPU##variant##_NOP(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert(instruction->addrMode == ADDRM_##variant##_IMP); \
} \
 \
void CPU##variant##_ORA(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	uint8_t m; \
	Read8(&m, cpu->bus, instruction->opu16); \
	cpu->a |= m; \
	cpu->status = cpu->status & 0XFD; \
	if (cpu->a > 255) \
	{ \
		cpu->status |= 0X82; \
	} \
	cpu->negative = (cpu->a & 0X80) == 0X80; \
	cpu->zero = !(cpu->a); \
} \
 \
void CPU##variant##_PHA(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert(instruction->addrMode == ADDRM_##variant##_SP); \
	Write8(cpu->a, cpu->bus, 0X100 | cpu->sp); \
	(cpu->sp)--; \
} \
 \
void CPU##variant##_PHP(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert(instruction->addrMode == ADDRM_##variant##_SP); \
	Write8(cpu->status, cpu->bus, 0X100 | cpu->sp); \
	(cpu->sp)--; \
} \
 \
void CPU##variant##_PLA(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert(instruction->addrMode == ADDRM_##variant##_SP); \
	(cpu->sp)++; \
	Read8(&(cpu->a), cpu->bus, 0X100 | cpu->sp); \
	cpu->status = (cpu->status & 0X7D) | (cpu->a & 0X80) | ((cpu->a == 0) << 1); \
} \
 \
void CPU##variant##_PLP(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert(instruction->addrMode == ADDRM_##variant##_SP); \
	(cpu->sp)++; \
	Read8(&(cpu->status), cpu->bus, 0X100 | cpu->sp); \
} \
 \
void CPU##variant##_ROL(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	uint8_t *m; \
	uint8_t value; \
	uint8_t carry = cpu->carry; \
	switch (instruction->addrMode) \
	{ \
		case ADDRM_##variant##_ACC: \
		{ \
			m = &(cpu->a); \
		} break; \
		default: \
		{ \
			m = GetPtrToAddr(cpu->bus, instruction->opu16); \
		} break; \
	} \
	value = *m; \
	cpu->carry = (value & 0X80) > 0; \
	value <<= 1; \
	value |= carry; \
	cpu->negative = (value & 0X80) > 0; \
	cpu->zero = (value == 0); \
	*m = value; \
} \
 \
void CPU##variant##_ROR(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	uint8_t *m; \
	uint8_t value; \
	uint8_t carry = cpu->carry; \
	switch (instruction->addrMode) \
	{ \
		case ADDRM_##variant##_ACC: \
		{ \
			m = &(cpu->a); \
		} break; \
		default: \
		{ \
			m = GetPtrToAddr(cpu->bus, instruction->opu16); \
		} break; \
	} \
	value = *m; \
	cpu->carry = value & 0X01; \
	value >>= 1; \
	value |= carry << 7; \
	cpu->negative = (value & 0X80) > 0; \
	cpu->zero = (value == 0); \
	*m = value; \
} \
 \
void CPU##variant##_RTI(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert(instruction->addrMode == ADDRM_##variant##_SP); \
	(cpu->sp)++; \
	Read8(&(cpu->status), cpu->bus, 0X100 | cpu->sp); \
	(cpu->sp)++; \
	Read8(&(cpu->pcl), cpu->bus, 0X100 | cpu->sp); \
	(cpu->sp)++; \
	Read8(&(cpu->pch), cpu->bus, 0X100 | cpu->sp); \
} \
 \
void CPU##variant##_RTS(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert(instruction->addrMode == ADDRM_##variant##_SP); \
	cpu->sp++; \
	Read8(&(cpu->pcl), cpu->bus, 0X100 | cpu->sp); \
	cpu->sp++; \
	Read8(&(cpu->pch), cpu->bus, 0X100 | cpu->sp); \
} \
 \
void CPU##variant##_SBC(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	uint8_t m; \
	uint16_t difference = 0; \
	Read8(&m, cpu->bus, instruction->opu16); \
	difference = cpu->a - m - ~(cpu->carry); \
	cpu->status = cpu->status & 0X7D; \
	if (difference > 255) \
	{ \
		cpu->status |= 0X82; \
	} \
	cpu->negative = (difference & 0X80) == 0X80; \
	cpu->a = difference & 0XFF; \
	cpu->zero = !(cpu->a); \
} \
 \
void CPU##variant##_SEC(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert(instruction->addrMode == ADDRM_##variant##_IMP); \
	cpu->carry = 1; \
} \
 \
void CPU##variant##_SED(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert(instruction->addrMode == ADDRM_##variant##_IMP); \
	cpu->decimal = 1; \
} \
 \
void CPU##variant##_SEI(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert(instruction->addrMode == ADDRM_##variant##_IMP); \
	cpu->irqDisable = 1; \
} \
 \
void CPU##variant##_STX(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	uint8_t *m = GetPtrToAddr(cpu->bus, instruction->opu16); \
	*m = cpu->x; \
} \
 \
void CPU##variant##_STY(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	uint8_t *m = GetPtrToAddr(cpu->bus, instruction->opu16); \
	*m = cpu->y; \
} \
 \
void CPU##variant##_TAX(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert((instruction->addrMode == ADDRM_##variant##_IMP)); \
	cpu->x = cpu->a; \
	cpu->negative = (cpu->x & 0X80) == 0X80; \
	cpu->zero = cpu->x == 0; \
} \
 \
void CPU##variant##_TAY(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert((instruction->addrMode == ADDRM_##variant##_IMP)); \
	cpu->y = cpu->a; \
	cpu->negative = (cpu->y & 0X80) == 0X80; \
	cpu->zero = cpu->y == 0; \
} \
 \
void CPU##variant##_TSX(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert((instruction->addrMode == ADDRM_##variant##_IMP)); \
	cpu->x = cpu->sp; \
	cpu->negative = (cpu->x & 0X80) == 0X80; \
	cpu->zero = cpu->x == 0; \
} \
 \
void CPU##variant##_TXA(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert((instruction->addrMode == ADDRM_##variant##_IMP)); \
	cpu->a = cpu->x; \
	cpu->negative = (cpu->a & 0X80) == 0X80; \
	cpu->zero = cpu->a == 0; \
} \
 \
void CPU##variant##_TXS(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert((instruction->addrMode == ADDRM_##variant##_IMP)); \
	cpu->sp = cpu->x; \
} \
 \
void CPU##variant##_TYA(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	assert((instruction->addrMode == ADDRM_##variant##_IMP)); \
	cpu->a = cpu->y; \
	cpu->negative = (cpu->a & 0X80) == 0X80; \
	cpu->zero = cpu->a == 0; \
} \
 \
void CPU##variant##_ResetDefault(CPU##variant *cpu) \
{ \
	CPU##variant##_Reset(cpu, 0b00010100); \
} \
 \
void CPU##variant##_Reset(CPU##variant *cpu, uint8_t status) \
{ \
	Read8(&(cpu->pcl), cpu->bus, 0XFFFC); \
	Read8(&(cpu->pch), cpu->bus, 0XFFFD); \
	cpu->a = 0; \
	cpu->x = 0; \
	cpu->y = 0; \
	cpu->sp = 0X00; \
	cpu->status = status; \
	cpu->irqSet = 0; \
	cpu->nmiSet = 0; \
} \
 \
void CPU##variant##_IRQ(CPU##variant *cpu) \
{ \
	if (cpu->irqDisable | !cpu->irqSet) return; \
	Write8(cpu->pch, cpu->bus, 0X100 | cpu->sp); \
	cpu->sp--; \
	Write8(cpu->pcl, cpu->bus, 0X100 | cpu->sp); \
	cpu->sp--; \
	Write8(cpu->status, cpu->bus, 0X100 | cpu->sp); \
	cpu->sp--; \
	cpu->irqDisable = 1; \
	Read8(&(cpu->pcl), cpu->bus, 0XFFFE); \
	Read8(&(cpu->pch), cpu->bus, 0XFFFF); \
} \
 \
void CPU##variant##_NMI(CPU##variant *cpu) \
{ \
	if (!cpu->nmiSet) return; \
	Write8(cpu->pch, cpu->bus, 0X100 | cpu->sp); \
	cpu->sp--; \
	Write8(cpu->pcl, cpu->bus, 0X100 | cpu->sp); \
	cpu->sp--; \
	Write8(cpu->status, cpu->bus, 0X100 | cpu->sp); \
	cpu->sp--; \
	cpu->irqDisable = 1; \
	Read8(&(cpu->pcl), cpu->bus, 0XFFFA); \
	Read8(&(cpu->pch), cpu->bus, 0XFFFB); \
} \

#define Impl6502AbsIndAddrMode(variant) \
void New_CPU##variant##Instruction_AbsoluteIndirect( \
	CPU##variant##Instruction *dst, \
	uint8_t instruction, \
	CPU##variant *cpu \
) \
{ \
	union { \
		uint8_t u8s[2]; \
		uint16_t u16; \
	} operand; \
	(cpu->pc)++; \
	Read8(operand.u8s, cpu->bus, cpu->pc); \
	(cpu->pc)++; \
	Read8(operand.u8s + 1, cpu->bus, cpu->pc); \
	cpu->pc = *((uint16_t*)GetPtrToAddr(cpu->bus, operand.u16)); \
	dst->ticks = 5; \
	dst->addrMode = ADDRM_##variant##_ABS_IND; \
}

#define Impl6502STA(variant) \
void CPU##variant##_STA(CPU##variant *cpu, CPU##variant##Instruction *instruction) \
{ \
	uint8_t *m; \
	Write8(cpu->a, cpu->bus, instruction->opu16); \
} \


Impl6502(6502)
Impl6502AbsIndAddrMode(6502)
Impl6502STA(6502)
Impl6502(65C02)

void New_CPU65C02Instruction_IndexedIndirect(
	CPU65C02Instruction *dst,
	uint8_t instruction,
	CPU65C02 *cpu
)
{
	union {
		uint8_t u8s[2];
		uint16_t u16;
	} operand;
	Read8(operand.u8s, cpu->bus, cpu->pc);
	(cpu->pc)++;
	Read8(operand.u8s + 1, cpu->bus, cpu->pc);
	dst->opu16 = *((uint16_t*)GetPtrToAddr(cpu->bus, operand.u16));
	dst->ticks = 6;
	dst->addrMode = ADDRM_65C02_IDX_IND_X;
}

void New_CPU65C02Instruction_AbsoluteIndirect(
	CPU65C02Instruction *dst,
	uint8_t instruction,
	CPU65C02 *cpu
)
{
	union {
		uint8_t u8s[2];
		uint16_t u16;
	} operand;
	(cpu->pc)++;
	Read8(operand.u8s, cpu->bus, cpu->pc);
	(cpu->pc)++;
	Read8(operand.u8s + 1, cpu->bus, cpu->pc);
	cpu->pc = *((uint16_t*)GetPtrToAddr(cpu->bus, operand.u16));
	dst->ticks = 6;
	dst->addrMode = ADDRM_65C02_ABS_IND;
}

void New_CPU65C02Instruction_ZeroPageIndirect(
	CPU65C02Instruction *dst,
	uint8_t instruction,
	CPU65C02 *cpu
)
{
	uint8_t operand;
	(cpu->pc)++;
	Read8(&operand, cpu->bus, cpu->pc);
	dst->opu16 = *(uint16_t*)GetPtrToAddr(cpu->bus, operand);
	dst->ticks = 5 + (operand == 0XFF);
	dst->addrMode = ADDRM_65C02_ZP_IND;
}

void CPU65C02_BBR(CPU65C02 *cpu, CPU65C02Instruction *instruction)
{
	assert(instruction->addrMode == ADDRM_65C02_PCR);
	uint8_t data;
	Read8(&data, cpu->bus, instruction->opu16);
	cpu->pc += instruction->opi8 *
		((data & (1 << (instruction->byte >> 4))) == 0);
}

void CPU65C02_BBS(CPU65C02 *cpu, CPU65C02Instruction *instruction)
{
	assert(instruction->addrMode == ADDRM_65C02_PCR);
	uint8_t data;
	Read8(&data, cpu->bus, instruction->opu16);
	cpu->pc += instruction->opi8 *
		((data & (1 << (instruction->byte >> 5))) > 0);
}

void CPU65C02_BRA(CPU65C02 *cpu, CPU65C02Instruction *instruction)
{
	assert(instruction->addrMode == ADDRM_65C02_PCR);
	cpu->pc += instruction->opi8;
}

void CPU65C02_PHX(CPU65C02 *cpu, CPU65C02Instruction *instruction)
{
	assert(instruction->addrMode == ADDRM_65C02_SP);
	Write8(cpu->x, cpu->bus, 0X100 | cpu->sp);
	(cpu->sp)--;
}

void CPU65C02_PHY(CPU65C02 *cpu, CPU65C02Instruction *instruction)
{
	assert(instruction->addrMode == ADDRM_65C02_SP);
	Write8(cpu->y, cpu->bus, 0X100 | cpu->sp);
	(cpu->sp)--;
}

void CPU65C02_PLX(CPU65C02 *cpu, CPU65C02Instruction *instruction)
{
	assert(instruction->addrMode == ADDRM_65C02_SP);
	(cpu->sp)++;
	Read8(&(cpu->x), cpu->bus, 0X100 | cpu->sp);
	cpu->status = (cpu->status & 0X7D) | (cpu->x & 0X80) | ((cpu->x == 0) << 1);
}

void CPU65C02_PLY(CPU65C02 *cpu, CPU65C02Instruction *instruction)
{
	assert(instruction->addrMode == ADDRM_65C02_SP);
	(cpu->sp)++;
	Read8(&(cpu->y), cpu->bus, 0X100 | cpu->sp);
	cpu->status = (cpu->status & 0X7D) | (cpu->y & 0X80) | ((cpu->y == 0) << 1);
}

void CPU65C02_RMB(CPU65C02 *cpu, CPU65C02Instruction *instruction)
{
	if (instruction->addrMode != ADDRM_65C02_ZP)
	{
		assert(0);
	}
	uint8_t *m = GetPtrToAddr(cpu->bus, instruction->opu16);
	*m = (*m) & ~(1 << ((instruction->byte & 0XF0) >> 4));
}

void CPU65C02_SMB(CPU65C02 *cpu, CPU65C02Instruction *instruction)
{
	assert(instruction->addrMode == ADDRM_65C02_ZP);
	uint8_t *m = GetPtrToAddr(cpu->bus, instruction->opu16);
	*m = (*m) | (1 << ((instruction->byte & 0XF0) >> 5));
}

void CPU65C02_STA(CPU65C02 *cpu, CPU65C02Instruction *instruction)
{
	uint8_t *m;
	switch (instruction->addrMode)
	{
		case ADDRM_65C02_IDX_IND_X:
		case ADDRM_65C02_ABS_IND:
		case ADDRM_65C02_ACC:
		case ADDRM_65C02_IMM:
		case ADDRM_65C02_IMP:
		case ADDRM_65C02_PCR:
		case ADDRM_65C02_SP:
		case ADDRM_65C02_ZP_IDX_Y:
		{
			assert(0);
		} break;
		case ADDRM_65C02_IDX_X:
		{
			instruction->ticks += 1;
		}
		default:
		{
			Write8(cpu->a, cpu->bus, instruction->opu16);
		} break;
	}
}

void CPU65C02_STP(CPU65C02 *cpu, CPU65C02Instruction *instruction)
{
	assert(instruction->addrMode == ADDRM_65C02_PCR);
}

void CPU65C02_STZ(CPU65C02 *cpu, CPU65C02Instruction *instruction)
{
	uint8_t *m;
	switch (instruction->addrMode)
	{
		case ADDRM_65C02_IDX_IND_X:
		case ADDRM_65C02_IDX_Y:
		case ADDRM_65C02_ABS_IND:
		case ADDRM_65C02_ACC:
		case ADDRM_65C02_IMM:
		case ADDRM_65C02_IMP:
		case ADDRM_65C02_PCR:
		case ADDRM_65C02_SP:
		case ADDRM_65C02_ZP_IND_X:
		case ADDRM_65C02_ZP_IDX_Y:
		case ADDRM_65C02_ZP_IND:
		case ADDRM_65C02_ZP_IND_Y:
		{
			assert(0);
		} break;
		default:
		{
			m = GetPtrToAddr(cpu->bus, instruction->opu16);
		} break;
	}
	*m = 0;
}

void CPU65C02_TRB(CPU65C02 *cpu, CPU65C02Instruction *instruction)
{
	uint8_t *m;
	uint8_t result;
	switch (instruction->addrMode)
	{
		case ADDRM_65C02_ABS:
		case ADDRM_65C02_ZP:
		{
			m = GetPtrToAddr(cpu->bus, instruction->opu16);
		} break;
		default:
		{
			assert(0);
		} break;
	}
	result = *m;
	cpu->zero = (cpu->a & result) == 0;
	result = ~(cpu->a) & result;
	*m = result;
}

void CPU65C02_TSB(CPU65C02 *cpu, CPU65C02Instruction *instruction)
{
	uint8_t *m;
	uint8_t result;
	switch (instruction->addrMode)
	{
		case ADDRM_65C02_ABS:
		case ADDRM_65C02_ZP:
		{
			m = GetPtrToAddr(cpu->bus, instruction->opu16);
		} break;
		default:
		{
			assert(0);
		} break;
	}
	result = *m;
	cpu->zero = (cpu->a & result) == 0;
	result = cpu->a | result;
	*m = result;
}

void CPU65C02_WAI(CPU65C02 *cpu, CPU65C02Instruction *instruction)
{
	assert((instruction->addrMode == ADDRM_65C02_IMP));
	cpu->pc -= cpu->irqSet - 1;
}
