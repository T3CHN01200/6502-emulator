#include "bus.h"
#include "cpu65C02.h"
#include "memory.h"
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>

typedef struct termios termios;

typedef union
{
	uint8_t u8;
	struct
	{
		uint8_t stopped : 1;
	};
} ProgramStatus;

termios def;
termios raw;
ProgramStatus status = { 0 };

void handleSignal(int signal)
{
	switch (signal)
	{
		case (SIGCONT):
		{
			if (status.stopped)
			{
				tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw);
				status.stopped = 0;
			}
		} break;
		case (SIGSTOP):
		case (SIGTSTP):
		case (SIGTTIN):
		case (SIGTTOU):
		{
			if (!status.stopped)
			{
				tcsetattr(STDIN_FILENO, TCSAFLUSH, &def);
				status.stopped = 1;
			}
		} break;
		case (SIGABRT):
		case (SIGBUS):
		case (SIGFPE):
		case (SIGHUP):
		case (SIGILL):
		case (SIGINT):
		case (SIGKILL):
		case (SIGPIPE):
		case (SIGQUIT):
		case (SIGSEGV):
		case (SIGTERM):
		case (SIGUSR1):
		case (SIGUSR2):
		case (SIGPOLL):
		case (SIGPROF):
		case (SIGSYS):
		case (SIGXCPU):
		case (SIGXFSZ):
		{
			tcsetattr(STDIN_FILENO, TCSAFLUSH, &def);
			exit(1);
		} break;
	}
}

int32_t main(int32_t argc, char **argv)
{
	{
		signal(SIGCONT, &handleSignal);
		signal(SIGSTOP, &handleSignal);
		signal(SIGTSTP, &handleSignal);
		signal(SIGTTIN, &handleSignal);
		signal(SIGTTOU, &handleSignal);
		signal(SIGABRT, &handleSignal);
		signal(SIGBUS, &handleSignal);
		signal(SIGFPE, &handleSignal);
		signal(SIGHUP, &handleSignal);
		signal(SIGILL, &handleSignal);
		signal(SIGINT, &handleSignal);
		signal(SIGKILL, &handleSignal);
		signal(SIGPIPE, &handleSignal);
		signal(SIGQUIT, &handleSignal);
		signal(SIGSEGV, &handleSignal);
		signal(SIGTERM, &handleSignal);
		signal(SIGUSR1, &handleSignal);
		signal(SIGUSR2, &handleSignal);
		signal(SIGPOLL, &handleSignal);
		signal(SIGPROF, &handleSignal);
		signal(SIGSYS, &handleSignal);
		signal(SIGXCPU, &handleSignal);
		signal(SIGXFSZ, &handleSignal);
	}
	tcgetattr(STDIN_FILENO, &def);
	raw = def;
	raw.c_lflag &= ~(ECHO | ICANON);
	raw.c_oflag &= ~(OPOST);
	tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw);
	uint8_t *mem = malloc(0X6000 + 0X8000);
	CPU65C02 cpu = { 0 };
	Bus bus = { 0 };
	Tickable tickables[] = {
		{
			.object = &cpu,
			.Tick = Tick_CPU65C02_void,
		},
	};
	Addressable blocks[] = {
		{
			.blockStartAddress = 0X0000,
			.blockEndAddress = 0X5FFF,
			.memory = mem + 0X0000,
			.blockType = BLOCK_TYPE_RAM,
		},
		{
			.blockStartAddress = 0X6000,
			.blockEndAddress = 0X7FFF,
			.blockType = BLOCK_TYPE_STDIO,
		},
		{
			.blockStartAddress = 0X8000,
			.blockEndAddress = 0XFFFF,
			.memory = mem + 0X6000,
			.blockType = BLOCK_TYPE_ROM,
		},
	};
	FILE *fIn = fopen(argv[1], "rb");
	fread(blocks[2].memory, 0X8000, 1, fIn);
	fclose(fIn);
	cpu.bus = &bus;
	bus.tickableCount = 1;
	bus.tickables = tickables;
	bus.blockCount = 3;
	bus.blocks = blocks;
	bus.waitForClock = 1;
	/* bus.clockNanos = 1000 / 21.477272; // NES NTSC */
	bus.clockNanos = 1000 / 10; // Eater Machine
	/* bus.clockNanos = 1000000000 / 10; // Slow clock for debug */
	CPU65C02_ResetDefault(&cpu);
	for (; cpu.brk;)
	{
		Tick_Bus(&bus);
	}
	tcsetattr(STDIN_FILENO, TCSAFLUSH, &def);
	printf("\r\n");
	free(mem);
	return 0;
}
