#!/bin/bash

PROJ_NAME=65C02-emu
CCOMPILER=clang
COMMONCFLAGS=" \
--include-directory include \
-static \
--save-temps=obj \
-std=c17 \
-target x86_64-pc-linux-musl \
"
DEBUGCFLAGS=" \
-glldb \
-O0 \
"
RELEASECFLAGS=" \
-DNDEBUG \
-O2 \
"
FILES=$(find src -name "**.c" | xargs -d "\n" realpath | tr "\n" " ")

mkdir -p build/{debug,release}/{bin,obj}

case $1 in
	"ccommands")
		echo "[$(
			eval "$CCOMPILER -MJ /dev/stdout -o /dev/null $COMMONCFLAGS $DEBUGCFLAGS $FILES" 2> /dev/null
		)]" > build/compile_commands.json
		;;
	"debug")
		eval "$CCOMPILER $COMMONCFLAGS -o build/$1/obj/$PROJ_NAME $DEBUGCFLAGS $FILES"
		mv build/$1/obj/$PROJ_NAME build/$1/bin/$PROJ_NAME
		;;
	"release")
		eval "$CCOMPILER $COMMONCFLAGS -o build/$1/obj/$PROJ_NAME $RELEASECFLAGS $FILES"
		mv build/$1/obj/$PROJ_NAME build/$1/bin/$PROJ_NAME
		;;
	"clean")
		rm -rf build
		;;
esac
